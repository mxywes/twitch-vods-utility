<?php 
/**
 * 
 */
class Vods
{
	

	function file_get_contents_curl($url='')
	{
		$ch = curl_init();
		curl_setopt_array($ch, array(
				CURLOPT_HTTPHEADER     => array(
						'Client-ID: um9vuwj2w3k3bhrwo8t10hva9dzpsa',
						'Accept: application/vnd.twitchtv.v5+json',
				),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_URL            => $url,
		));
		$response = curl_exec($ch);
		echo curl_error($ch);
		curl_close($ch);

		return $response;
	}

	function var_dump_pre($mixed = null) {
  		echo '<pre>';
 		 var_dump($mixed);
 		 echo '</pre>';
 		 return null;
	}
}
 ?>