<!doctype html>
<html>
<head><link rel="icon" type="img/ico" href="/../favicon2.ico">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="lib/noty.min.js" defer></script>
<script src="js/script.js" defer></script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="lib/noty.css">
<link rel="stylesheet" type="text/css" href="lib/themes/relax.css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<!-- <script type="text/javascript">(new Image()).src = "png/icons8-game-controller-24.png";(new Image()).src = "png/icons8-calendar-24.png";(new Image()).src = "png/icons8-clock-24.png";</script>
 -->
<?php
require_once('vods.class.php');
if (!isset($vodsclass)) {
	$vodsclass = new Vods;
}

function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

$clientID = "um9vuwj2w3k3bhrwo8t10hva9dzpsa";
// IS SUBMITTED?

$channel = $url = "";
if (!empty($_COOKIE['channel']) && empty($_REQUEST["channel"])) {
	$channel = $_COOKIE['channel'];
	$channel_from_cookie = true;
}

if (!empty($_REQUEST["channel"]) && !empty($_REQUEST)) {
		$channel = test_input($_REQUEST["channel"]);
		$channel_from_cookie = false;
		setcookie('channel',$channel);
}
if (isset($channel)) {
	?>
	<title>Twitch VODs of <?php echo ($channel) ?></title>
	<?php
} else {
	?>
	<title>Twitch VODs - Channel not set</title>
	<?php
}

if (!empty($_REQUEST["t"]) && !empty($_REQUEST)) {
		$type = test_input($_REQUEST["t"]);
}
?>

</head>

<?php

// var_dump_pre($_GET);
// var_dump_pre($_POST);

if (!empty($channel)) {
	$result = json_decode($vodsclass->file_get_contents_curl("https://api.twitch.tv/helix/users?login=".$channel), true);
	$user_id = $result['data'][0]['id'];
	if (!empty($type) && ($type=='h' or $type=='u')) {
		$url = 'https://api.twitch.tv/kraken/channels/' . $user_id . '/videos?limit=24&type=upload,highlight';
	}
	else {
		$url = 'https://api.twitch.tv/kraken/channels/' . $user_id . '/videos?limit=24&type=archive';
	}
}
else {
	$url = null;
}

//LAAA

$white_logo_div = '<div><a href="https://id.twitch.tv/oauth2/authorize?client_id='.$clientID.'&redirect_uri=https://'. htmlspecialchars($_SERVER['SERVER_NAME']).htmlspecialchars($_SERVER["PHP_SELF"]).'&response_type=code&scope=user_read"><img style="height: 32px; width: 32px;" src="https://cdn.collabo.gq/img/twitch/GlitchIcon_White_48px.png"></a></div>';
$purple_logo_div = '<div><a href="https://id.twitch.tv/oauth2/authorize?client_id='.$clientID.'&redirect_uri=https://'. htmlspecialchars($_SERVER['SERVER_NAME']).htmlspecialchars($_SERVER["PHP_SELF"]).'&response_type=code&scope=user_read"><img style="height: 32px; width: 32px;" src="https://cdn.collabo.gq/img/twitch/GlitchIcon_Purple_48px.png"></a></div>';

$code = $token = "";

if ((!empty($_REQUEST["code"]) && !empty($_REQUEST))) {
		$code = $_REQUEST["code"];
		$data = array('client_id' => $clientID, 'code' => $code, 'client_secret' => 'okxr6288c6c5vc6u5q9rqutt1rgadh', 'grant_type' => 'authorization_code', 'redirect_uri' => 'https://' . htmlspecialchars($_SERVER['SERVER_NAME']).htmlspecialchars($_SERVER["PHP_SELF"]));
		$options = array('http' => array('header'  => "Content-type: application/x-www-form-urlencoded\r\n", 'method'  => 'POST', 'content' => http_build_query($data)));
		$context  = stream_context_create($options);
		$result = @file_get_contents('https://id.twitch.tv/oauth2/token', false, $context);
		if ($result === FALSE) { 
			$twitch_login = false;
			$login_message = '<span><abbr title="Outdated OAuth : click logo again or remove token from URL">Token error</abbr></span>';
		}
		else {
			$access_token = json_decode($result)->access_token;
			$ch = curl_init();
			curl_setopt_array($ch, array(
				CURLOPT_HTTPHEADER     => array(
				'Client-ID: um9vuwj2w3k3bhrwo8t10hva9dzpsa',
				'Authorization: OAuth '.$access_token,
				),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_URL            => 'https://api.twitch.tv/kraken/user',
			));
			$response = curl_exec($ch);
			curl_close($ch);
			$user_json = json_decode($response);
			$twitch_login = true;
			// echo "<span>".$user_json->display_name."</span>";
			$login_message = $user_json->name;
			$json_array = json_decode($vodsclass->file_get_contents_curl('https://api.twitch.tv/kraken/users/'.$user_json->name.'/follows/channels?limit=100&sortby=login&direction=asc'), true);
			$offset2=0;
			$follows = array();
			foreach ($json_array['follows'] as $key => $value) {
					$follows[$key+$offset2] = $value['channel']['name'];
			}
			while (($json_array['_total']-$offset2) > 100) {
				$offset2+=100;
				$json_array = json_decode($vodsclass->file_get_contents_curl('https://api.twitch.tv/kraken/users/'.$user_json->name.'/follows/channels?limit=100&direction=asc&sortby=last_broadcast&offset='.$offset2), true);
				foreach ($json_array['follows'] as $key => $value) {
					$follows[$key+$offset2] = $value['channel']['name'];
				}
			}
			
			if ($json_array['_total']>100) {
				
			}
			$follows = base64_encode(json_encode($follows));
			setcookie('follows', $follows, 0, '', '', isset($_SERVER["HTTPS"]), false);
		}
		
		// var_dump();

} elseif (isset($_COOKIE['follows'])) {
	$twitch_login = true;
	$login_message = "<span>Imported from cookie</span>";
} else {
	$twitch_login = false;
	$login_message = "<span>Import follows</span>";
}







?>

<body>
<div class="container-fluid">

<?php 
if (isset($url)) {
	?>
	<div id='header' class='row text-white justify-content-start' style="margin-top: 1rem;">
	<div class="col-sm-4 title-header-container">
		<h3 class="title-head">
			<span ondblclick="eraseCookie('channel');eraseCookie('follows');window.location.href = 'index.php'" style="cursor: pointer;">Twitch VODs <abbr title="developped by @Mxywes">utility</abbr></span>
		</h3>
		<?php if ($channel_from_cookie) echo('<span class="title-head-toast"><input id="channel_from_cookie" type="hidden" value="true"></span>'); ?>
	</div>
	<div class="col-sm-4">
	<form class="form-inline justify-content-center" method="post" autocomplete="off" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
	<label class="sr-only" for="inlineChannelInput">Channel</label><div class="autocomplete"><input class="black-bg form-control" type="text" id="inlineChannelInput" name="channel" list="follows" placeholder="Channel" <?php if (isset($url)) {echo 'value="'.$channel.'"';} ?>>
		<?php 
		if (isset($_COOKIE['follows']) || isset($follows)) {
			echo('<datalist id="follows">');
			$follows = (isset($_COOKIE['follows'])) ? json_decode(base64_decode($_COOKIE['follows'])) : $follow ;
			foreach ($follows as $key => $value) {
				?>
				<option value="<?php echo($value); ?>">	
				<?php
			}
		echo('</datalist>');
		} ?>
	</div>
	<button type="submit" class="btn btn-dark" name="submit">Submit</button>
	</form>
	</div>
	<div class="col-sm-4">
		<div class="row">
			<div class="col-sm-9">
			<form class="form-inline justify-content-center">
			<label class="sr-only" for="inlineQualityInput">Quality</label>
			<select class="form-control black-bg" id="inlineQualityInput">
			<option selected value="best">best</option>
			<option value="1080p60">1080p60</option>
			<option value="720p60">720p60</option>
			<option value="720p">720p</option>
			<option value="480p">480p</option>
			<option value="360p">360p</option>
			<option value="Audio_Only">Audio_Only</option>
			<option value="direct_link">Direct Link</option>
			</select>
			</form>
			<!-- <span style="font-size: 0.8rem;">Needs <a href="https://addons.mozilla.org/en-US/firefox/addon/native_hls_playback/">Native HLS Playback</a> extension for direct link streams (for now)</span> --></div>
			<div style="text-align: center;" class="col-sm-3">				
				<?php 
				if ($twitch_login) {
					echo($purple_logo_div);
				}
				else {
					echo($white_logo_div);
				}
				echo($login_message);
				?>
			</div>
		</div>
		
		
	</div>
	</div>
	<?php
} 
else {
	?>
	<div id='header' class='row'>
	<div class="container text-center" style="margin-top: 0.7rem;">
	<form class="form-inline justify-content-center" method="post" autocomplete="off" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
	<label class="sr-only" for="inlineChannelInput">Channel</label>
	<div class="autocomplete">
		<input class="form-control" type="text" id="inlineChannelInput" name="channel" list="follows" placeholder="Channel" <?php if (isset($url)) {echo 'value="'.$channel.'"';} ?>>
	</div>
	<button type="submit" class="btn btn-dark" name="submit">Submit</button>
	</form>
	</div>
	</div>
	<?php
}

 ?>



<div id="wrapper" class="row text-white" style="margin-top: 30px;">

<?php
if (isset($url)) {
		include dirname(__FILE__) . '/request.php';
}
else {
	?>

	<h3 class="sum">Twitch VODs <abbr title="developped by @Mxywes">utility</abbr>
	<small class="text-muted">Get viewing and download links of any Twitch channel videos</small></h3>

	<?php
} 
?> 

<!-- onclick="copyToClipboard(\'url' . $i . '\')" -->
</div>
<div class="text-center">
<div class="spinner-border text-secondary m-4" id="spinner" style="display: none; width: 3rem; height: 3rem; border: .30em solid currentColor; border-right-color: transparent;" role="status">
  <span class="sr-only">Loading next...</span>
</div>
</div>
</div>
</body>
</html> 