<?php 
header("Access-Control-Allow-Origin: *");
$url = (isset($_GET['url'])) ? $_GET['url'] : null ;

if (empty($url)) {
  echo "No url provided, don't access this page directly";
}
else {
 ?>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>
<div id="video"></div>
<script>
    var player = new Clappr.Player({source: "<?php echo $url; ?>", parentId: "#video"});
</script>
<!-- <script src="js/hls.js"></script>
<video id="video"></video>
<script>
  var video = document.getElementById('video');
  if(Hls.isSupported()) {
    var hls = new Hls();
    hls.loadSource('<?php echo $url; ?>');
    hls.attachMedia(video);
    hls.on(Hls.Events.MANIFEST_PARSED,function() {
      video.play();
  });
 }
  else if (video.canPlayType('application/vnd.apple.mpegurl')) {
    video.src = '<?php echo $url; ?>';
    video.addEventListener('loadedmetadata',function() {
      video.play();
    });
  }
</script> -->
<img src="">
<?php
}
?>