Twitch VODs utility
====================
Get viewing and download links of any Twitch channel videos.
[Preview](http://ymaxy.gq/random/vods/index.php) (not up to date)
[Collabo server (require auth)](https://collabo.gq/public/tvu/index.php)

## Planned

- Branch new-api :
	* [] Pagination
	* [] Thumbnails
	* [] Account auth
- Current :
	* [] Homemade pagination

## Current Issues

Due to recent shutdown of Kraken v3, most functionalities are broken with v5.
- Fetching next videos (infinite scrolling) due to pagination link disappearing in the API (both v5 and future Helix)
- Everything related to Twitch account auth


## Changes


### v1.4.1
* Direct links open in browser on mobile (any compatible player app will trigger intent like VLC or MX Player)
### v1.4
* Kraken v5 migration with Helix for user-id retrieve
* Several functionalities broken (account auth, scrolling...)
* Added support for custom thumbnails
### v1.3
* Download link using youtube-dl server-side, opening in mpv:// protocol (need client configuration)
* Notifications reworked
### v1.2
* Twitch account auth, import user follows (not working in current version)
### v1.1
* Prioritize POST value over old cookie value
* Notification set up
### v1
Init

## Credits

- [NOTY](https://github.com/needim/noty) Copyright (c) 2012 Nedim Arabacı
    [MIT LICENSE](https://github.com/needim/noty/blob/master/LICENSE.txt)
- [youtube-dl](https://github.com/ytdl-org/youtube-dl) Unlicensed
