<?php 
require_once('vods.class.php');
if (!isset($vodsclass)) {
	$vodsclass = new Vods;
}

// function var_dump_pre($mixed = null) {
// 	echo '<pre>';
// 	var_dump($mixed);
// 	echo '</pre>';
// 	return null;
// }
setlocale(LC_TIME, "fr_FR");
if (!empty($_GET['u'])) {
	$url = $_GET['u'];
}
if (!empty($_GET['o'])) {
	$offset = $_GET['o'];
}
else {
	$offset = 0;
}

if (!empty($url)) {
	$json_array = json_decode($vodsclass->file_get_contents_curl($url), true);
	// $vodsclass->var_dump_pre($json_array);
	extract($json_array, EXTR_OVERWRITE);
	// $vodsclass->var_dump_pre($videos, EXTR_OVERWRITE);
	if (!empty($videos)) {
		for ($i = 0, $size = count($videos); $i < $size; ++$i) {	
			extract($videos[$i], EXTR_OVERWRITE);
			$thumb = [];
			// str_replace(["%{width}","%{height}"], ["320","120"], $thumbnail_url);
			if (strpos($preview['medium'], 'custom')!==false) {
				for ($j = 0; $j < 4; $j++) {
					$thumb[$j] = $preview['medium'];
				}
			}
			else {
				for ($j = 0; $j < 4; $j++) {
					$thumb[$j] = substr_replace($preview['medium'], $j, -13, 1);
				}
			}
			

			// $thumb = [];
			// $thumb[0] = substr_replace($thumbnails[0]["url"], "320x180", -12, -5);
			// for ($j = 1; $j < count($thumbnails); $j++) {
			// 	$thumb[$j] = substr_replace($thumbnails[$j]["url"], "320x180", -11, -4);
			// }
				
			?>
			<div id="card-container" class="col-sm-6 col-md-4 col-lg-3 col-xl-2 mb-3" name="card-container">
			<div class="card text-white bg-dark hoverable" id="card<?php echo $i+$offset; ?>">
			<?php 
			if ($status!='recorded') {
				echo('<img src="' . $preview . '" class="card-img-top" alt="preview-' . ($i+(int)$offset) . '">');
			}
			else {
				echo('<img src="' . $thumb[0] . '" class="card-img-top" alt="preview-' . ($i+(int)$offset) . '" id="processed">');
				for ($j = 0; $j < count($thumb); $j++) {
					echo ('<input type="hidden" id="' . $j . '" value="' . $thumb[$j] . '">');
				}
			}
			?>
								
			<div class="card-body">
			<h5 class="card-title" id="titleContainer"><small id="titleText"><?php echo $title; ?></small></h5>
			<span class="card-text smaller">
			<ul class="list-group list-group-flush smaller">
			<li class="list-group-item" id ="date"><img class="preload-icons" src="png/icons8-calendar-24.png"> <span class="align-text-bottom" id="dateText"><?php echo (ucwords(strftime("%a %x %H:%M:%S", strtotime($created_at)))); ?></span></li>
			<li class="list-group-item"><img class="preload-icons" src="png/icons8-clock-24.png"> <span class="align-text-bottom"><?php echo (date('H:i:s', mktime(0, 0, $length))); ?></span></li>
			<li class="list-group-item" id="game"><img class="preload-icons" src="png/icons8-game-controller-24.png"> <span class="align-text-bottom" id="gameText"><?php echo $game; ?></span></li>
			</ul>
			</span>
			<a class="url card-link" id="url<?php echo $i+(int)$offset; ?>" href="<?php echo $url; ?>"><img class="preload-icons" src="png/icons8-twitch-24.png"> Twitch Link</a>
			<a class="url card-link" id="ytdl<?php echo $i+(int)$offset; ?>" href="javascript:;" onclick="downloadLink('<?php echo $url; ?>');"><img class="preload-icons" src="png/icons8-tv-show-24.png"> Direct Link</a>
			<!-- <span class="url mx-auto" id="url<?php echo $i+$offset; ?>"><?php echo $url; ?></span> -->
			</div>
			</div>
			</div>
			<?php
		}
		$offset += $i;
		echo '<input type="hidden" id="offset" value="'.$offset.'">';
		echo '<input type="hidden" id="total" value="'.$json_array['_total'].'">';
		?>
		<a class="pagination__next" style="display: none;" href="<?php echo($json_array['_links']['next']); ?>">Next</a>
		<?php	
	}
	else {
		echo "This user has no videos or does not exist.";

	}

}
else {
	echo "Pas d'url fournie";
};
?>