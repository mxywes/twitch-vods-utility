// Debug mode
var debug_mode = false;
// Remove third-party host watermark
var third_party_host = false;

var allImagesLoaded=true, c=0, imgs = document.querySelectorAll('img.card-img-top'), len = imgs.length;

function eraseCookie(name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}

function copyToClipboard(elementId) {
	var aux = document.createElement("input");
	aux.setAttribute("value", document.getElementById(elementId).innerHTML);
	document.body.appendChild(aux);
	aux.select();
	document.execCommand("copy");
	document.body.removeChild(aux);
}
function resizeText(containerId, elementId) {
	var containers = document.querySelectorAll('#'+containerId);
	for (var i = 0; i < containers.length; i++) {
		var element = containers[i].querySelector('#'+elementId);

		while (element.offsetHeight > containers[i].offsetHeight) {
			var style = window.getComputedStyle(element), baseFontSize = style.getPropertyValue('font-size');
			if ((baseFontSize.replace('px','') < 12) && (element.offsetHeight > containers[i].offsetHeight)) {
				containers[i].setAttribute('style','overflow: auto;');
				break;
			}
			element.setAttribute('style','font-size: '+((baseFontSize.replace('px','')-0.15)+'px')+'; ');
		}
	}
}
function resetTextSize(containerId, elementId) {
	var containers = document.querySelectorAll('#'+containerId);
	for (var i = 0; i < containers.length; i++) {
			var element = containers[i].querySelector('#'+elementId);
			element.removeAttribute('style');
			containers[i].removeAttribute('style');
	}
}
function addMouseEvents(containers,inc) {
	var thumbs = [];
	var thumbtimer, ti = 0;
	for (var i = 0; i < containers.length; i++) {
		var list_input = containers[i].firstElementChild.querySelectorAll('input');
		var thumbs2 = [];
		for (var j = 0; j < list_input.length; j++) {
				thumbs2[thumbs2.length] = list_input[j].getAttribute("value");
				(new Image()).src = list_input[j].value;
				list_input[j].parentNode.removeChild(list_input[j]);
		}
		thumbs[i+parseInt(inc)] = thumbs2;
		var img = containers[i].firstElementChild.querySelector('img#processed');
		if (img!=null) {
			img.setAttribute("id", i+parseInt(inc));
			img.addEventListener("mouseover", function(event) {
					event.target.src = thumbs[event.target.id][1];
					thumbtimer = setInterval(function() {
						ti++;
						ti = (ti == thumbs[event.target.id].length) ? 0 : ti;
						event.target.src = thumbs[event.target.id][ti];
					}, 400);
				});
			img.addEventListener("mouseout", function(event) {
				clearInterval(thumbtimer);
				ti = 0;
				event.target.src = thumbs[event.target.id][ti];
			});
		}	
	}
	if (debug_mode) console.log("Events added to images");
}
function initInfiniteScrolling() {
	window.addEventListener('scroll', function () {
		var wrapper = document.getElementById('wrapper');
		if (window.pageYOffset+window.innerHeight >= wrapper.offsetHeight && allImagesLoaded==true) {
			if (debug_mode) console.log('End of page reached, requesting new cards...');
			allImagesLoaded = false;
			toggleSpinner();
			fetchNextCards(document.getElementsByClassName('pagination__next')[0].href);	
		}
	});
}
function fetchNextCards(next_url) {
	var offset = document.getElementById('offset').getAttribute('value');
	var offset = document.getElementById('total').getAttribute('value');
	let xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
    if (this.readyState == 4	) {
      var parser = new DOMParser(), htmldoc = parser.parseFromString(this.responseText, 'text/html');
			addMouseEvents(htmldoc.querySelectorAll("div#card-container"), offset);
			var cardContainer = htmldoc.querySelectorAll('#card-container');
			imgs = htmldoc.getElementsByTagName('body')[0].querySelectorAll('img.card-img-top');
			var input = htmldoc.querySelector('input#offset'),
			a = htmldoc.querySelector('a.pagination__next'),
			wrapper = document.querySelector('#wrapper');
			if (debug_mode) console.log('All cards added!');
			for (var i = 0; i < cardContainer.length; i++) {
				wrapper.appendChild(cardContainer[i]);
			}
			wrapper.removeChild(wrapper.querySelector('input#offset'));
			wrapper.removeChild(wrapper.querySelector('a.pagination__next'));
			wrapper.appendChild(input);
			wrapper.appendChild(a);
			checkImagesLoaded();
			resizeAll();
    }
    };
	xhr.open('GET','request.php?o='+offset+'&u='+encodeURIComponent(next_url));
	xhr.send();
}
function downloadLink(url) {
	var quality = document.getElementById('inlineQualityInput').value;
	new Noty({
    	text: 'Fetching direct link from Twitch...',
    	timeout: false,
    	progressBar: true,
    	id: 'ytdlnotif',
    	type: 'alert',
    	theme: 'relax'
     }).show();
	let xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (this.readyState == 4) {
			// window.open('player.php?url='+encodeURIComponent(this.responseText));
			document.getElementById('ytdlnotif').click();
			new Noty({
		    	text: 'Stream opening in mpv...',
		    	timeout: 5000,
		    	progressBar: true,
		    	id: 'mpvnotif',
		    	type: 'alert',
		    	theme: 'relax'
	     	}).show();
	     	if (!window.matchMedia("only screen and (max-width: 760px)").matches) {
	     		window.location = 'mpv://'+this.responseText;
	     	}
	     	else {
	     		window.location = this.responseText;
	     	}
			
		}
	}
	// xhr.open('GET', 'ytdl.php?video='+encodeURIComponent(url)+'&quality=best');
	xhr.open('GET', 'ytdl.php?video='+encodeURIComponent(url)+'&quality='+quality);
	xhr.send();
}
function incrementCounter(e) {
	c++;
	if (c >= (len)) {
		if (debug_mode) console.log( 'All images loaded!' );
		allImagesLoaded = true;
		toggleSpinner();
		c=0;
	}
	e.target.removeEventListener(e.type, arguments.callee)
}
function checkImagesLoaded() {
	len = imgs.length, c=0;
	[].forEach.call(imgs, function(img) {
		img.addEventListener('load',incrementCounter,false);
	}
	);
}
function toggleSpinner() {
	var spin = document.getElementById('spinner');
	if (spin.style.display=='none') {
		spin.style.display = 'inline-block';
	}
	else {
		spin.style.display = 'none';
	};
}
function resizeAll() {
	resetTextSize('titleContainer','titleText')
	resetTextSize('game','gameText');
	resetTextSize('date','dateText');
	resizeText('titleContainer','titleText')
	resizeText('game','gameText');
	resizeText('date','dateText');
}

window.onload = () => {
	addMouseEvents(document.querySelectorAll("div#card-container"), 0);

	if (document.getElementById("channel_from_cookie")=="true") {
		new Noty({
    	text: 'Loaded from cookie',
    	timeout: 2500,
    	progressBar: true,
    	id: 'notifCookie',
    	type: 'alert',
    	container: 'span.title-head-toast',
    	theme: 'relax'
     }).show();
	}

	if (third_party_host) {
		let el = document.querySelector('[alt="www.000webhost.com"]').parentNode.parentNode;
		el.parentNode.removeChild(el);
	}

	resizeText('game','gameText');
	resizeText('titleContainer','titleText');
	resizeText('date','dateText');

};

window.onresize = function(event) {
		if (debug_mode) console.log('window resized');
		resizeAll();
};



initInfiniteScrolling();

